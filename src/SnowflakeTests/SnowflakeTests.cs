﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Snowflake.Client;
using Snowflake.Data.Client;

namespace SnowflakeTests
{
    [TestClass]
    public class SnowflakeTests
    {
        private string username = "raymondchan";
        private string password = "*masked*";
        private string warehouse = "SEGMENT_WAREHOUSE";
        private string database = "SEGMENT_EVENTS_QA";
        private string schema = "RAYMOND_NET_SOURCE_DEV";
        private string account = "nx05651";
        private string region = "canada-central.azure";
        private string host = "nx05651.canada-central.azure.snowflakecomputing.com";

        [TestMethod]
        public void Should_connect_to_snowflake_using_adonet_connector()
        {
            // Snowflake Connector - install via nuget package
            // https://github.com/snowflakedb/snowflake-connector-net

            int i = 0;
            try
            {
                using (IDbConnection conn = new SnowflakeDbConnection())
                {
                    string connstring = $"account={account};host={host};user={username};password={password};db={database};schema={schema};warehouse={warehouse}";
                    conn.ConnectionString = connstring;
                    conn.Open();
                    Console.WriteLine("Connection successful!");
                    using (IDbCommand cmd = conn.CreateCommand())
                    {

                        // select * from SEGMENT_EVENTS_QA.RAYMOND_NET_SOURCE_DEV.IN_STORE_PICKUP
                        // cmd.CommandText = "select * from SEGMENT_EVENTS_QA.RAYMOND_NET_SOURCE_DEV.IN_STORE_PICKUP";   

                        /*
                            select
                                ID,
                                ,USER_ID
                                ,SHIPPING_ADDRESS_STREET
                                ,EVENT
                                ,DOVER_ID
                                ,ORDER_TOTAL
                            from SEGMENT_EVENTS_QA.RAYMOND_NET_SOURCE_DEV.IN_STORE_PICKUP
                         */
                        cmd.CommandText = "select ID, USER_ID, SHIPPING_ADDRESS_STREET, EVENT, DOVER_ID, ORDER_TOTAL from SEGMENT_EVENTS_QA.RAYMOND_NET_SOURCE_DEV.IN_STORE_PICKUP";
                        Console.WriteLine("ID, USER_ID, SHIPPING_ADDRESS_STREET, EVENT, DOVER_ID, ORDER_TOTAL");
                        //data from an existing table
                        IDataReader reader = cmd.ExecuteReader();
    
                        while (reader.Read())
                        {
                            i++;
                            Console.WriteLine(reader.GetString(0) + ", " + reader.GetString(1) + ", " + reader.GetString(2) + ", " + reader.GetString(3) + ", " + reader.GetString(4) + ", " + reader.GetString(5));
                            if (i > 10) 
                                break;
                        }

                        conn.Close();
                    }
                }
            }
            catch (DbException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        [TestMethod]
        public void Should_connect_to_snowflake_using_client_package()
        {
            // Snowflake Client
            // https://medium.com/snowflake/better-net-client-for-snowflake-db-ecb48c48c872
            // https://github.com/fixer-m/snowflake-db-net-client

            try
            {
                var snowflakeClient = new SnowflakeClient(username, password, account, region);

                
                var eventRecords = snowflakeClient.QueryAsync<InStorePickup>(
                                    // don't seem to need to specify the warehouse
                                "select ID, USER_ID, SHIPPING_ADDRESS_STREET, EVENT, DOVER_ID, ORDER_TOTAL from SEGMENT_EVENTS_QA.RAYMOND_NET_SOURCE_DEV.IN_STORE_PICKUP;")
                                    .Result;


                Console.WriteLine("ID, USER_ID, SHIPPING_ADDRESS_STREET, EVENT, DOVER_ID, ORDER_TOTAL");
                int i = 0;
                foreach (InStorePickup x in eventRecords)
                {
                    i++;
                    Console.WriteLine(x.ID + ", " + x.USER_ID + ", " + x.SHIPPING_ADDRESS_STREET + ", " + x.EVENT + ", " + x.DOVER_ID + ", " + x.ORDER_TOTAL);
                    if (i > 10)
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}

public class InStorePickup
{
    public string ID { get; set; }
    public string USER_ID { get; set; }
    public string SHIPPING_ADDRESS_STREET { get; set; }
    public string EVENT { get; set; }
    public string DOVER_ID { get; set; }
    public string ORDER_TOTAL { get; set; }
}