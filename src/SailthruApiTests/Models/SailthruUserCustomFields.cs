﻿using System;
using Newtonsoft.Json;

namespace SailthruApiTests.Models
{
    /// <summary>
    /// This class maps to Sailthru User Profile Custom Fields
    /// Which maps to our Indigo User Traits that we send in our Identify() calls to Segment
    /// </summary>
    public class SailthruUserCustomFields
    {
        [JsonProperty(PropertyName = "birthday")]
        public DateTime? Birthday { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty(PropertyName = "first_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "language")]
        public object Language { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "membership_id")]
        public string MembershipId { get; set; }

        [JsonProperty(PropertyName = "loyalty_number")]
        public long? LoyaltyNumber { get; set; }

        [JsonProperty(PropertyName = "loyalty_subscription_expiry_date")]
        public DateTime? LoyaltySubscriptionExpiryDate { get; set; }

        [JsonProperty(PropertyName = "loyalty_subscription_last_renewal_date")]
        public DateTime? LoyaltySubscriptionLastRenewalDate { get; set; }

        [JsonProperty(PropertyName = "loyalty_type")]
        public string LoyaltyType { get; set; }
    }

}

