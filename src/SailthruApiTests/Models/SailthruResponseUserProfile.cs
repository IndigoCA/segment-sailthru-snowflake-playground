﻿using System;
using Newtonsoft.Json;

namespace SailthruApiTests.Models
{
    /// <summary>
    /// Sailthru User Profiles are represented as CustomFields with key value pairs
    /// </summary>
    public class SailthruResponseUserProfile
    {
        
        [JsonProperty(PropertyName = "keys")]
        public SailthruUserProfileOverview Overview { get; set; }

        [JsonProperty(PropertyName = "vars")]
        public SailthruUserCustomFields UserTraits { get; set; }
        public object Lists { get; set; }
        public string Engagement { get; set; }
        
        [JsonProperty(PropertyName = "optout_email")]
        public string OptoutEmail { get; set; }
    }
  
}

