﻿using System;
using Newtonsoft.Json;

namespace SailthruApiTests.Models
{
    /// <summary>
    /// This class maps to Sailthru User Profile Overview page
    /// </summary>
    public class SailthruUserProfileOverview
    {
        public string sid { get; set; }
        public string cookie { get; set; }
        public string email { get; set; }

        /// <summary>
        /// Sailthru external ID maps tou our Indigo CustomerId
        /// </summary>
        [JsonProperty(PropertyName = "extId")]
        public string CustomerId { get; set; } 

    }
}

