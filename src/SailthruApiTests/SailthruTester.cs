﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using Newtonsoft.Json;
using Sailthru;
using Sailthru.Models;
using SailthruApiTests.Models;

namespace SailthruApiTests
{

    /// <summary>
    /// Sample code to read from Sailthru
    /// https://getstarted.sailthru.com/developers/api-client/net/
    /// </summary>
    [TestClass]
    public class SailthruTester
    {
        private SailthruClient _sailthruClient;

        [TestInitialize]
        public void TestInitialize()
        {
            // Indigo DEV Sailthru environment apiKey
            // Log into Sailthru first, then you can find the keys here: https://my.sailthru.com/settings/api_postbacks#
            string apiKey = "*masked*"; 
            string secret = "*masked*";
            _sailthruClient = new SailthruClient(apiKey, secret);
        }

        [TestMethod]
        public void Should_get_user()
        {
            UserRequest userRequest = new UserRequest();
            userRequest.Id = "rchan1@indigo.ca";

            SailthruResponse response = _sailthruClient.GetUser(userRequest);

            if (response.IsOK())
            {
                var obj = JsonConvert.DeserializeObject(response.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                Console.WriteLine(formattedJson);


                // can also try to deserialize to our Indigo Model/poco class
                // however, the email under test may have various properties that don't match
                SailthruResponseUserProfile userProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(response.RawResponse);
                var userProfileJson = JsonConvert.SerializeObject(userProfile, Formatting.Indented);
                Console.WriteLine(userProfileJson);

            }
            else
            {
                Console.WriteLine($"User profile not found for: {userRequest.Id}");
                Console.WriteLine(response.RawResponse);
            }
        }
        [TestMethod]
        public void Should_delete_user()
        {
            Hashtable parameters = new Hashtable();
            parameters["id"] = "rchan5_indigo_ca+deleted@indigo.ca";
            SailthruResponse response = _sailthruClient.ApiDelete("user", parameters);

            if (response.IsOK())
            {
                var obj = JsonConvert.DeserializeObject(response.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                Console.WriteLine(formattedJson);

            }
            else
            {
                Console.WriteLine($"User not found for: '{parameters["id"]}'");
                Console.WriteLine(response.RawResponse);
            }

        }

        [TestMethod]
        public void Should_get_smart_list()
        {
            // https://getstarted.sailthru.com/developers/api/list/
            Hashtable parameters = new Hashtable();
            parameters["list"] = "Rays Smart List - user with \"trait1 = 1\"";
            SailthruResponse response = _sailthruClient.ApiGet("list", parameters);

            if (response.IsOK())
            {
                var obj = JsonConvert.DeserializeObject(response.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                Console.WriteLine(formattedJson);

            }
            else
            {
                Console.WriteLine($"User list not found for: '{parameters["list"]}'");
                Console.WriteLine(response.RawResponse);
            }

        }
        [TestMethod]
        public void Should_export_users_from_smart_list()
        {
            throw new NotImplementedException();

            // https://getstarted.sailthru.com/developers/api/job/#export-list-data
            Hashtable parameters = new Hashtable();
            parameters["list"] = "Rays Smart List - user with \"trait1 = 1\"";
            string jobType = "export_list_data";
            string reportEmail = "rchan1@indigo.ca";
            string indigoWebhookUrl = "https://webhooks.dev.indigo.ca/SailthruExportListData";
            SailthruResponse response = _sailthruClient.ProcessJob(jobType, reportEmail, indigoWebhookUrl, parameters);

            if (response.IsOK())
            {
                var obj = JsonConvert.DeserializeObject(response.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                Console.WriteLine(formattedJson);

            }
            else
            {
                Console.WriteLine($"User list not found for: '{parameters["list"]}'");
                Console.WriteLine(response.RawResponse);
            }

        }
    }
}
