﻿
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Segment;
using Segment.Model;
using System;
using System.Configuration;

namespace SegmentConsoleApp
{
    public class SegmentClient : Client, IDisposable
    {

        private bool _disposed = false;
        private static log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static SegmentClient _segmentClientInstance;
        private bool EnableDetailedLogging;

        private SegmentClient(string segmentWriteKey, Config segmentConfig, bool enableDetailedLogging) : base(segmentWriteKey, segmentConfig)
        {
            EnableDetailedLogging = enableDetailedLogging;
            this.Succeeded += SuccessHandler;
            this.Failed += FailureHandler;
            Logger.Handlers += LogHandler;
        }

        public static SegmentClient Instance
        {
            get
            {
                if (_segmentClientInstance == null)
                {
                    SegmentSettings settings = GetSettings();
                    Config config = GetConfig(settings);
                    _segmentClientInstance = new SegmentClient(settings.WriteKey, config, settings.EnableDetailedLogging);
                }

                return _segmentClientInstance;
            }
        }

        private static SegmentSettings GetSettings()
        {
            SegmentSettings segmentSettings = new SegmentSettings();

            if(string.IsNullOrEmpty(ConfigurationManager.AppSettings["SegmentWriteKey"]))
            {
                throw new ConfigurationErrorsException("Segment Write Key is mandatory");
            }

            segmentSettings.WriteKey = ConfigurationManager.AppSettings["SegmentWriteKey"];
            segmentSettings.MaxQueueSize = int.Parse(ConfigurationManager.AppSettings["SegmentMaxQueueSize"]);
            segmentSettings.FlushThreshold = int.Parse(ConfigurationManager.AppSettings["SegmentFlushThreshold"]);
            segmentSettings.FlushInterval = double.Parse(ConfigurationManager.AppSettings["SegmentFlushAtSeconds"]);
            segmentSettings.EnableGzip = bool.Parse(ConfigurationManager.AppSettings["SegmentEnableGzip"]);
            segmentSettings.EnableBatching = bool.Parse(ConfigurationManager.AppSettings["SegmentEnableBatching"]);
            segmentSettings.EnableDetailedLogging = bool.Parse(ConfigurationManager.AppSettings["SegmentEnableDetailedLogging"]);
            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["SegmentTimeoutMs"]))
                segmentSettings.Timeout = int.Parse(ConfigurationManager.AppSettings["SegmentTimeoutMs"]);
            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["SegmentMaxRetryMs"]))
                segmentSettings.MaxRetryTime = int.Parse(ConfigurationManager.AppSettings["SegmentMaxRetryMs"]);

            return segmentSettings;
        }

        private static Config GetConfig(SegmentSettings segmentSettings)
        {
            Config config = new Config();

            config.SetAsync(segmentSettings.EnableBatching);
            config.SetGzip(segmentSettings.EnableGzip);
            config.SetMaxQueueSize(segmentSettings.MaxQueueSize);
            config.SetFlushAt(segmentSettings.FlushThreshold);
            config.SetFlushInterval(segmentSettings.FlushInterval); 

            if (segmentSettings.Timeout != null)
                config.SetTimeout(TimeSpan.FromMilliseconds(double.Parse(segmentSettings.Timeout.ToString())));
            if (segmentSettings.MaxRetryTime != null)
                config.SetTimeout(TimeSpan.FromMilliseconds(double.Parse(segmentSettings.Timeout.ToString())));

            return config;
        }

        private void SuccessHandler(BaseAction action)
        {
            if (EnableDetailedLogging)
            {
                // we don't have these values
                // Dictionary<string, object> eventInfoDetails = (Dictionary<string, object>)action.Context["event_info"];
                // string segmentEventName = eventInfoDetails["segment_event_name"].ToString();
                // string orderId = eventInfoDetails["order_id"].ToString();
                // Log.Info(string.Format("Message sent successfully to Segment. Message info is as follows: MessageID: {0}, EventType: {1}, UserID: {2}, SegmentEventName: {3}, OrderId: {4} ", action.MessageId, action.Type, action.UserId, segmentEventName, orderId));
                Log.Info(string.Format("Message sent successfully to Segment. Message info is as follows: MessageID: {0}, EventType: {1}, UserID: {2}", action.MessageId, action.Type, action.UserId));
            }
        }

        private void FailureHandler(BaseAction action, Exception e)
        {
            // we don't have these values
            // Dictionary<string, object> eventInfoDetails = (Dictionary<string, object>)action.Context["event_info"];
            // string segmentEventName = eventInfoDetails["segment_event_name"].ToString();
            // string orderId = eventInfoDetails["order_id"].ToString();
            //Log.Error(string.Format("Message failed to be sent to Segment. Message info is as follows: MessageID: {0}, EventType: {1}, UserID: {2}, SegmentEventName: {3}, OrderId: {4} ", action.MessageId, action.Type, action.UserId, segmentEventName, orderId));
            Log.Error(string.Format("Message failed to be sent to Segment. Message info is as follows: MessageID: {0}, EventType: {1}, UserID: {2}", action.MessageId, action.Type, action.UserId));
        }

        private void LogHandler(Logger.Level level, string msg, IDictionary<string, object> args)
        {

            if (EnableDetailedLogging && (args != null))
            {
                msg = args.Keys.Aggregate(msg, (current, key) => current + $" {string.Empty + key}: {string.Empty + args[key]},");
            }

            switch (level)
            {
                case Logger.Level.DEBUG:
                    Log.Debug(msg);
                    break;
                case Logger.Level.INFO:
                    Log.Info(msg);
                    break;
                case Logger.Level.WARN:
                    Log.Warn(msg);
                    break;
                case Logger.Level.ERROR:
                    Log.Error(msg);
                    break;
            }
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            _disposed = true;

            Flush();
            base.Dispose();
        }

        public static void Shutdown()
        {
            if (_segmentClientInstance != null)
            {
                _segmentClientInstance.Dispose();
            }
        }

    }
}
