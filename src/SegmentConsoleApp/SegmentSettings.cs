﻿using System.Configuration;

namespace SegmentConsoleApp
{
    public class SegmentSettings
    {
        /// <summary>
        /// Enables the batching of messages going to Segment.
        /// </summary>
        public bool EnableBatching { get; set; }

        /// <summary>
        /// Compress data w/ gzip before dispatch.
        /// </summary>
        public bool EnableGzip { get; set; }

        /// <summary>
        /// The maximum number of messages to allow into the queue before no new message are accepted.
        /// </summary>
        public int MaxQueueSize { get; set; }

        /// <summary>
        /// The message threshold where Segment will flush the client.
        /// </summary>
        public int FlushThreshold { get; set; }

        /// <summary>
        /// The frequency, in seconds, to send data to Segment.
        /// </summary>
        public double FlushInterval { get; set; }

        /// <summary>
        /// Sets the maximum amount of timeout on the HTTP request flushes to the server
        /// </summary>
        public int? Timeout { get; set; }

        /// <summary>
        /// Max Amount of time to retry request when server timeout occurs.
        /// </summary>
        public int? MaxRetryTime { get; set; }

        /// <summary>
        /// Enables detailed logging of Segment messages.
        /// </summary>
        public bool EnableDetailedLogging { get; set; }

        /// <summary>
        /// The segment source write key.
        /// </summary>
        public string WriteKey { get; set; }

    }
}
