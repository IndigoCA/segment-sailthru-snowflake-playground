﻿using System;
using Segment;
using Segment.Model;
using System.Configuration;
using System.Net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace SegmentConsoleApp
{


    /// <summary>
    /// This program is a simple Console App that aims to replicate behavior of our other applications
    /// using the wrapper SegmentClient
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            logger.Info("Starting Application");

            // hack if we can't update OS
            // System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;


            IAnalyticsClient analyticsClient = SegmentClient.Instance;

            string userId = ConfigurationManager.AppSettings["Playground.UserId"];
            string eventName = ConfigurationManager.AppSettings["Playground.EventName"];
            string myproperty = ConfigurationManager.AppSettings["Playground.MyProperty"];

            Properties properties = new Properties();
            properties.Add("language", "en-ca");
            properties.Add("my_timestamp", DateTime.Now.ToString());
            properties.Add("my_property", myproperty);


            Context context = new Context()
            {
                {"traits", new Dict() {
                        {"email", "rchan1@indigo.ca"}
                    }
                }
            };
            Options options = new Options().SetContext(context);

            logger.Info("Calling Analytics.Net Client Track");
            analyticsClient.Track(userId, eventName, properties, options);

            logger.Info("Calling Analytics.Net Flush");
            //Analytics.Client.Flush(); // do we need this?

            Console.WriteLine("Hit enter to end");
            Console.ReadLine();
            logger.Info("End Application");
        }
    }
}
