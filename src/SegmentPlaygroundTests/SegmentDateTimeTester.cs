﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Segment;
using Segment.Model;

namespace SegmentPlaygroundTests
{
    [TestClass]
    public class SegmentDateTimeTester
    {
        
        public void Initialize_With_Standard_Configs()
        {
            /* Common values in config files, these should be default values
                <add key="SegmentWriteKey" value="" />
                <add key="SegmentMaxQueueSize" value="100000" />
                <add key="SegmentFlushThreshold" value="40" />
                <add key="SegmentEnableGzip" value="false" />
                <add key="SegmentEnableBatching" value="true" />
            */
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/
            int maxQueueSize = 100000;
            int flushThreshold = 40;
            bool enableGzip = false;
            bool enableBatching = false; // for testing to make this synchronous

            Config config = new Config()
                .SetMaxQueueSize(maxQueueSize)
                .SetFlushAt(flushThreshold)
                .SetGzip(enableGzip)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        [TestMethod]
        public void Should_send_Identify_call_with_date_time_values()
        {

            Initialize_With_Standard_Configs();

            DateTime unspecifiedDate = new DateTime(1980, 09, 05); // running this in Toronto on Oct 5, 2021 which is EDT (no daylight savings)
            DateTime edtDate = new DateTime(1980, 09, 05, 0, 0, 0, DateTimeKind.Local);
            DateTime utcDate = new DateTime(1980, 09, 05, 0, 0, 0, DateTimeKind.Utc);
            DateTime unspecifiedDateTime  = new DateTime(2021, 10, 22, 10, 0, 1);
            DateTime edtDateTime = new DateTime(2021, 10, 22, 10, 2, 3, DateTimeKind.Local);
            DateTime edtDateTimeWithoutTime = DateTime.SpecifyKind(new DateTime(2021, 10, 22), DateTimeKind.Local);
            DateTime edtDateTimeNowUnspecified = DateTime.Now;
            DateTime edtDateTimeNowLocal = DateTime.SpecifyKind(edtDateTimeNowUnspecified, DateTimeKind.Local);
            DateTime utcDateTime = new DateTime(2021, 10, 22, 10, 4, 4, DateTimeKind.Utc);
            DateTime? nullableDateTimeAsNull = null;
            DateTime? nullableDateTimeWithValue = new DateTime(2021, 07, 16);

            DateTimeOffset offsetMinus5hr = new DateTimeOffset(2007, 04, 05, 06, 07, 08, new TimeSpan(5, 0, 0));
            DateTimeOffset offsetMinus4hr = new DateTimeOffset(2007, 04, 05, 06, 07, 08, new TimeSpan(4, 0, 0));
            DateTimeOffset offsetMinus0hr = new DateTimeOffset(2007, 04, 05, 06, 07, 08, new TimeSpan(0,0,0));
            DateTimeOffset offsetMinus30min = new DateTimeOffset(2007, 04, 05, 06, 07, 08, new TimeSpan(0, 30, 0));

            string userId = "RayChan99";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Testing Date Formats"},
                {"email", "rchan_date1@indigo.ca"},
                {"unspecified_date", unspecifiedDate},
                {"edt_date", edtDate},
                {"utc_date", utcDate},
                {"unspecifiedDateTime", unspecifiedDateTime},
                {"edtDateTime", edtDateTime},
                {"edtDateTime_without_time", edtDateTimeWithoutTime},
                {"edtDateTimeNow_unspecified", edtDateTimeNowUnspecified},
                {"edtDateTimeNow_local", edtDateTimeNowLocal},

                {"utcDateTime", utcDateTime},
                {"nullable_datetime_with_null", nullableDateTimeAsNull},
                {"nullable_datetime_with_value", nullableDateTimeWithValue},

                {"offset_minus_5hr", offsetMinus5hr},
                {"offset_minus_4hr", offsetMinus4hr},
                {"offset_minus_0hr", offsetMinus0hr},
                {"offset_minus_30min", offsetMinus30min},
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        //----------------------------------------------------------------------------
        public void FailureHandler(BaseAction action, System.Exception e)
        {
            // e.g. storeId to log this to NewRelic
            // string storeId = action.Context["event_info"]["storeId"];
            Console.WriteLine($"Failure: userid={action.UserId}"); // we can print out other stuff here too
            Console.WriteLine(e.ToString());
        }

        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"Success: userid={action.UserId}"); // we can print out other stuff here too
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {message}");
        }
    }
}
