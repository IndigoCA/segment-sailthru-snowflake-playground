﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Segment;
using Segment.Model;

namespace SegmentPlaygroundTests
{
    [TestClass]
    public class SegmentWrapperIdeas
    {
        /* Ideal code if we had interfaces, we could just swap out the implementation

        public void ExampleCallOldCode()
        {
            // Indigo.Emailer.IEmailer emailer = new Indigo.Emailer.EspiSender()

            // Indigo.Emailer.IEmailer emailer = new Indigo.Emailer.SegmentSender()

            List<string> users = new List<string>() {"user123", "user456"};
            foreach (string userId in users)
            {
                try
                {
                    Console.WriteLine($"Processing {userId} started");
                    emailer.SendEmail(userId, orderGraph, userObject, templateFields);
                    Console.WriteLine($"Processing {userId} completed");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Processing {userId} FAILED. {e.Message}");
                }
            }
        }
         */

   

        [TestMethod]
        public void ExampleCallOldCode()
        {
            List<string> users = new List<string>() {"user123", "user456"};
            foreach (string userId in users)
            {
                try
                {
                    Console.WriteLine($"Processing {userId} started");
                    DoSomethingSynchronously(userId);
                    Console.WriteLine($"Processing {userId} completed");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Processing {userId} FAILED. {e.Message}");
                }
            }
        }

        public void DoSomethingSynchronously(string userId)
        {
            if (userId.Contains("123"))
                Console.WriteLine($"Doing something with {userId}");
            else
                throw new Exception("UserId needs to contain 123.");
        }


        [TestMethod]
        public void ExampleNewCode()
        {
            // Segment initialization and payload setup
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/
            Config configWithBatchingOff = new Config().SetAsync(false);
            Segment.Analytics.Initialize(writeKey, configWithBatchingOff);

            Context context = new Context() {{"appName", "MyApp"}};
            Options optionsContext = new Options().SetContext(context);
            Properties properties = new Properties() {{"my_first_prop", "foo bar"}};

            // setup callback handlers
            Analytics.Client.Failed += FailureHandler;
            Analytics.Client.Succeeded += SuccessHandler;
            Logger.Handlers += LogHandler;

            // the old code structure I want to preserve
            List<string> users = new List<string>() { "user123", "user456" };
            foreach (string userId in users)
            {
                try
                {
                    Console.WriteLine($"Processing {userId} started");
                    Analytics.Client.Track(userId, "Test Fired", properties, optionsContext);
                    Console.WriteLine($"Processing {userId} completed");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Processing {userId} FAILED. {e.Message}");
                }

                // lets say this is program exit
                Analytics.Client.Flush();

                // do processing, e.g. log how many succeed, how many failed, and which ones
            }
        }

        public void FailureHandler(BaseAction action, System.Exception e)
        {
            Console.WriteLine($"FailureHandler called for userid={action.UserId}");
            Console.WriteLine($"FailureHandler called for context={action.Context.ToString()}");
            Console.WriteLine(e.ToString());
        }
        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"SuccessHandler called for userid={action.UserId}");
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {message}");
        }

    }
}
