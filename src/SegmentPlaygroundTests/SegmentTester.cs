﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Segment;
using Segment.Model;

namespace SegmentPlaygroundTests
{
    [TestClass]
    public class SegmentTester
    {
        [TestMethod]
        public void Demo_Send_Identify()
        {
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/
            int flushThreshold = 20; // flush after every 20 requests
            double flushInterval = 10; // flush every 10 seconds
            bool enableBatching = false; // for testing, make this synchronous

            Config config = new Config()
                .SetFlushAt(flushThreshold)
                .SetFlushInterval(flushInterval)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            string userId = "UnitTest789";
            Traits traits = new Segment.Model.Traits()
            {
                {"email", "unittest789@indigo.ca"},
                {"first_name", "Unit"},
                {"last_name", "Test"},
                {"address", new Dict() {
                    {"street", "620 King Street"},
                    {"city", "Toronto"}}
                }
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }
        [TestMethod]
        public void Demo_Send_Track()
        {
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/
            int flushThreshold = 20; // flush after every 20 requests
            double flushInterval = 10; // flush every 10 seconds
            bool enableBatching = false; // for testing, make this synchronous

            Config config = new Config()
                .SetFlushAt(flushThreshold)
                .SetFlushInterval(flushInterval)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            string userId = "RayChan123456";
            Context context = new Context()
            {
                {"traits", new Dict() {
                        {"email", "rchan1@indigo.ca"}
                    }
                }
            };
            Options optionsContext = new Options().SetContext(context);

            string eventName = "Testing Opt Out";
            Analytics.Client.Track(userId, eventName, optionsContext);
            Analytics.Client.Flush();
        }


        public void Initialize_With_Standard_Configs()
        {
            /* Common values in config files, these should be default values
                <add key="SegmentWriteKey" value="" />
                <add key="SegmentMaxQueueSize" value="100000" />
                <add key="SegmentFlushThreshold" value="40" />
                <add key="SegmentEnableGzip" value="false" />
                <add key="SegmentEnableBatching" value="true" />
            */
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/
            int maxQueueSize = 100000;
            int flushThreshold = 40;
            bool enableGzip = false;
            bool enableBatching = false; // for testing to make this synchronous

            Config config = new Config()
                .SetMaxQueueSize(maxQueueSize)
                .SetFlushAt(flushThreshold)
                .SetGzip(enableGzip)
                .SetAsync(enableBatching);


            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        [TestMethod]
        public void Should_send_Identify_call()
        {
            Initialize_With_Standard_Configs();

            string userId = "RayChan123456";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Unit Test"},
                {"email", "rchan1@indigo.ca"},
                {"address", new Dict() {
                        {"street", "620 King Street"},
                        {"city", "Toronto"}}
                }
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Should_send_Track_call()
        {
            Initialize_With_Standard_Configs();

            string userId = "RayChan123456";

            // setup context fields for Track call
            Context context = new Context() 
            {
                {"appName", "SegmentPlayground"},

                // setup the traits for identity resolution (if we had 
                {"traits", new Dict() {
                        {"email", "rchan1@indigo.ca"}
                    }
                }
            };
            
            Options optionsContext = new Options().SetContext(context);

            // setup Track call
            string eventName = "Siddharth Test";
            Properties properties = new Properties()
            {
                {"orderSummary", new Properties() {
                        {"firstName", "Raymond"},
                        {"lastName", "Test"}
                    }
                },
                {"shippingAddress", new Properties() {
                        { "street", "620 King Street" },
                        { "city", "Toronto" }
                    }
                },
                {"DoverId", "1111"},
                {"ShopOrderId", "8888888"},
                {"RootOrderId", "99999999"},
                {"IsSplitShipment", "true"},
                {"products", new Properties() {
                        {"product", new Properties() {
                                {"catalog", "gardening" },
                                {"productId", "fa5b5571-c63d-4b5a-b9b0-765d5591c14e" },
                                {"sku", "978123456789" },
                                {"title", "How to grow a mango tree" },
                                {"author", "Mr know it all" },
                                {"format", "hardcover" },
                                {"quantity", "1" },
                                {"promoName", "10% off father's day" },
                                {"estimatedDeliveryDate", "2021-06-29" },
                                {"itemSubTotal", "29.53"}
                            }
                        }
                    }
                },
                {"orderTotal", "95.03"},

                // Common Properties
                {"type", "transactional_email"},
                {"timestamp", DateTime.Now.ToString("yyyy-MM-dd")},
                {"version", "1.0"}
            };
            Analytics.Client.Track(userId, eventName, properties, optionsContext);
            Analytics.Client.Flush();
        }


        [TestMethod]
        public void Should_send_OptIn_Track_call()
        {
            Initialize_With_Standard_Configs();

            string userId = "RayChan123456";

            // setup context fields for Track call
            Context context = new Context()
            {
                {"appName", "SegmentPlayground"},
                {"traits", new Dict() {
                        {"email", "rchan1@indigo.ca"}
                    }
                }
            };
            Options optionsContext = new Options().SetContext(context);

            // setup Track call
            string eventName = "Email OptedIn";
            Properties properties = new Properties()
            {
                {"email", "rchan1@indigo.ca"}, // not really needed

            };
            Analytics.Client.Track(userId, eventName, properties, optionsContext);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Should_send_OptOut_Track_call()
        {
            Initialize_With_Standard_Configs();

            string userId = "RayChan123456";

            // setup context fields for Track call
            Context context = new Context()
            {
                {"appName", "SegmentPlayground"},
                {"traits", new Dict() {
                        {"email", "rchan1@indigo.ca"}
                    }
                }
            };
            Options optionsContext = new Options().SetContext(context);

            // setup Track call
            string eventName = "Email OptedOut";
            Properties properties = new Properties()
            {
                {"email", "rchan1@indigo.ca"}, // not really needed

            };
            Analytics.Client.Track(userId, eventName, properties, optionsContext);
            Analytics.Client.Flush();
        }

        //----------------------------------------------------------------------------
        [TestMethod]
        public void Should_throw_exception_with_null_userId()
        {
            // this doesn't even make it to Segment... nuget package throws exception
            Initialize_With_Standard_Configs();
            string userId = null;
            Analytics.Client.Track(userId, "", null, null);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Should_throw_exception_with_null_eventName()
        {
            // this doesn't even make it to Segment... nuget package throws exception
            Initialize_With_Standard_Configs();
            string userId = null;
            Analytics.Client.Track(userId, "", null, null);
            Analytics.Client.Flush();
        }
        [TestMethod]
        public void Should_throw_exception_with_no_write_key()
        {
            string writeKey = "";
            Config config = new Config();
            Segment.Analytics.Initialize(writeKey, config);
            /*
             Message: 
            Test method SegmentPlaygroundTests.SegmentTester.Should_throw_exception_with_no_write_key threw exception: 
            System.InvalidOperationException: Please supply a valid writeKey to initialize.

              Stack Trace: 
            Client.ctor(String writeKey, Config config)
            Analytics.Initialize(String writeKey, Config config)
            SegmentTester.Should_throw_exception_with_no_write_key() line 213
             */
        }

        [TestMethod]
        public void Should_send_to_Segment_with_no_errors_with_bad_write_key()
        {
            string writeKey = "this_is_not_a_valid_key";
            Config config = new Config();
            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;

            string userId = "RayChan123456";
            Analytics.Client.Track(userId, "raytest", null, null);
            Analytics.Client.Flush();
            /*
                [DEBUG] Enqueued action in async loop. message id: 0015d1e3-2afa-40db-8938-dc4d2b3ccd34, queue size: 1,
                [DEBUG] Dequeued action in async loop. message id: 0015d1e3-2afa-40db-8938-dc4d2b3ccd34, queue size: 0,
                [DEBUG] Created flush batch. batch size: 1,
                [INFO] Sending analytics request to Segment.io .. batch id: 9cd977cc-1432-4df2-9b4d-145cd43b795f, json size: 384, batch size: 1,
                Success: userid=RayChan123456
                [INFO] Segment.io request successful. batch id: 9cd977cc-1432-4df2-9b4d-145cd43b795f, duration (ms): 643,
             */
        }

        [TestMethod]
        public void Should_fail_to_send_when_payload_greater_than_32kb_for_regular_api_call()
        {
            // TODO: I can't seem to make this a regular API call
            // probably means Analytics Package defaults to batch call
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/
            Config config = new Config().SetAsync(false); // single call, not batch call
            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;

            string userId = "RayChan123456";

            // roughly, 1 character = 8 bytes -- 32KB = need 32*1024 characters
            string verylongstring = new string('*', 32*1024);
            Properties properties = new Properties()
            {
                {"long_string", verylongstring }
            };
            Analytics.Client.Track(userId, "raytest", properties, null);
            Analytics.Client.Flush();
        }
        [TestMethod]
        public void Should_fail_to_send_when_payload_greater_than_500kb_for_batch_api_call()
        {
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/
            Config config = new Config().SetAsync(false); 
            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;

            string userId = "RayChan123456";

            // roughly, 1 character = 8 bytes -- 500KB = need 512*1024 characters
            string verylongstring = new string('*',512 * 1024);
            Properties properties = new Properties()
            {
                {"long_string", verylongstring }
            };
            Analytics.Client.Track(userId, "raytest", properties, null);
            Analytics.Client.Flush();

            /* getting this message - looks like Analytics package rejecting before sending to Segment
                  Standard Output: 
                [INFO] Sending analytics request to Segment.io .. batch id: 2a26331e-2120-409e-84e2-a4d20d7acc3a, json size: 524688, batch size: 1,
                Failure: userid=RayChan123456
                Segment.Exception.APIException: Status Code: 400, Message: Has Backoff reached max: False with number of Attempts:0,
                 Status Code: 400
                , response message: 
                [INFO] Segment.io request failed. batch id: 2a26331e-2120-409e-84e2-a4d20d7acc3a, reason: Status Code: 400, Message: Has Backoff reached max: False with number of Attempts:0,
                 Status Code: 400
                , response message: , duration (ms): 917,
             */
        }
        //----------------------------------------------------------------------------
        public void Initialize_To_Cause_Timeout_Errors()
        {
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/

            Config config = new Config()
                .SetAsync(false)
                .SetTimeout(TimeSpan.FromMilliseconds(1))
                .SetMaxRetryTime(TimeSpan.FromMilliseconds(1));

            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        [TestMethod]
        public void Should_cause_timeout_and_make_failure_handler_be_called()
        {
            Initialize_To_Cause_Timeout_Errors();

            string userId = "RayChanXYZ";
            Context context = new Context()
            {
                {"appName", "SegmentPlayground"},
                {"traits", new Dict() {
                        {"email", "rchan1@indigo.ca"}
                    }
                }
            };
            Options optionsContext = new Options().SetContext(context);
            string eventName = "Timeout Happened";
            Properties properties = new Properties() { };
            Analytics.Client.Track(userId, eventName, properties, optionsContext);
            Analytics.Client.Flush();
        }


        //----------------------------------------------------------------------------
        public void FailureHandler(BaseAction action, System.Exception e)
        {
            // e.g. storeId to log this to NewRelic
            // string storeId = action.Context["event_info"]["storeId"];
            Console.WriteLine($"Failure: userid={action.UserId}"); // we can print out other stuff here too
            Console.WriteLine(e.ToString());
        }

        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"Success: userid={action.UserId}"); // we can print out other stuff here too
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {message}");
        }
    }
}
