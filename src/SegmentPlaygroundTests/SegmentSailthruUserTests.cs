﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Segment;
using Segment.Model;

namespace SegmentPlaygroundTests
{
    [TestClass]
    public class SegmentSailthruUserTests
    {
        [TestInitialize]
        public void Initialize()
        {
            string writeKey = "*masked*"; // instructions on how to find: https://segment.com/docs/connections/find-writekey/
            int maxQueueSize = 100000;
            int flushThreshold = 40;
            bool enableGzip = false;
            bool enableBatching = false; // for testing to make this synchronous

            Config config = new Config()
                .SetMaxQueueSize(maxQueueSize)
                .SetFlushAt(flushThreshold)
                .SetGzip(enableGzip)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        [TestMethod]
        public void Test01_send_same_userId_but_different_email_1()
        {
            string userId = "userId_ray";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Ray Test"},
                {"email", "email_ray@indigo.ca"},
                {"trait1", "trait1_value" },
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test01_send_same_userId_but_different_email_2()
        {
            string userId = "userId_ray";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Ray Test"},
                {"email", "email_ray_diff@indigo.ca"},
                {"trait1", "trait1_value_diff" },
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }
        //-----------

        [TestMethod]
        public void Test02_send_same_anonymousId_but_different_email_1()
        {
            string userId = null;
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "John Test"},
                {"email", "email_john@indigo.ca"},
                {"trait1", "trait1_value" },
            };
            Options options = new Options().SetAnonymousId("anon_john");
            Analytics.Client.Identify(userId, traits, options);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test02_send_same_anonymousId_but_different_email_2()
        {
            string userId = null;
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "John Test"},
                {"email", "email_john_diff@indigo.ca"},
                {"trait1", "trait1_value_diff" },
            };
            Options options = new Options().SetAnonymousId("anon_john");
            Analytics.Client.Identify(userId, traits, options);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test02_send_same_anonymousId_but_different_email_3()
        {
            string userId = "userId_john";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "John Test"},
                {"email", "email_john_another@indigo.ca"},
                {"trait1", "trait1_value_another" },
            };
            Options options = new Options().SetAnonymousId("anon_john");
            Analytics.Client.Identify(userId, traits, options);
            Analytics.Client.Flush();
        }
        //---
        [TestMethod]
        public void Test03_send_different_userId_but_same_email_1()
        {
            string userId = "userId_bob";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Bob Test"},
                {"email", "email_bob@indigo.ca"},
                {"trait1", "trait1_value" },
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test03_send_different_userId_but_same_email_2()
        {
            string userId = "userId_bob_diff";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Bob Test"},
                {"email", "email_bob@indigo.ca"},
                {"trait1", "trait1_value_diff" },
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test04_send_same_userId_but_different_email_1()
        {
            string userId = "userId_kate";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Ray Test"},
                {"email", "email_kate@indigo.ca"},
                {"trait1", "trait1_value" },
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test04_send_same_userId_but_different_email_2()
        {
            string userId = "userId_kate";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Ray Test"},
                {"email", ""}, // clear it out
                {"trait1", "trait1_value_diff" },
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test04_send_same_userId_but_different_email_3()
        {
            string userId = "userId_kate";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "Ray Test"},
                {"trait1", "trait1_value_diff" }, // email not passed in
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test05_send_same_userId_update_traits_1()
        {
            string userId = "userId_george";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "George Test"},
                {"email", "email_george@indigo.ca"}, 
                {"membership_id", "123456" },
                {"first_name", "George" },
                {"last_name", "Test" },
                {"birthdate", new DateTime(1970, 1, 1) }
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        [TestMethod]
        public void Test05_send_same_userId_update_traits_2()
        {
            string userId = "userId_george";
            Traits traits = new Segment.Model.Traits()
            {
                {"name", "George Test"},
                {"email", "email_george@indigo.ca"},
                {"membership_id", "" },
                {"first_name", null },
                {"last_name", "" },
                {"birthdate", null }
            };
            Analytics.Client.Identify(userId, traits);
            Analytics.Client.Flush();
        }

        //----------------------------------------------------------------------------
        public void FailureHandler(BaseAction action, System.Exception e)
        {
            // e.g. storeId to log this to NewRelic
            // string storeId = action.Context["event_info"]["storeId"];
            Console.WriteLine($"Failure: userid={action.UserId}"); // we can print out other stuff here too
            Console.WriteLine(e.ToString());
        }

        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"Success: userid={action.UserId}"); // we can print out other stuff here too
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {message}");
        }
    }
}
