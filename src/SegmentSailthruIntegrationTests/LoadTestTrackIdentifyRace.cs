﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Sailthru;
using Sailthru.Models;
using Segment;
using Segment.Model;
using SegmentSailthruIntegrationTests.Models;

namespace SegmentSailthruIntegrationTests
{
    /// <summary>
    /// Sailthru implemented auto-creation of User Profile in Track() call for anonymous_id support
    /// This load test is to see what happens when we send, in batch, identify() + track() calls
    /// 
    /// e.g. if the track() call gets executed first
    /// it will create a user profile (e.g. email abc@my.com, extId=123)
    /// then, identify() call will also try to create the same profile
    /// we could see an error
    ///
    /// NOTE was not able to produce any race condition errors
    /// </summary>
    //[TestClass] // uncomment to run
    public class LoadTestTrackIdentifyRace
    { 

        public void Initialize_Segment_with_standard_configuration()
        {
            string writeKey = "masked"; // make sure to pick the correct environment/source!
            int maxQueueSize = 100000;
            int flushThreshold = 40;
            bool enableGzip = false;
            bool enableBatching = true; 

            Config config = new Config()
                .SetMaxQueueSize(maxQueueSize)
                .SetFlushAt(flushThreshold)
                .SetGzip(enableGzip)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        public SailthruClient GetSailthruClient()
        {
            // Log into Sailthru first (make sure to pick the correct environment)
            // then you can find the keys here: https://my.sailthru.com/settings/api_postbacks#
            string apiKey = "masked"; 
            string secret = "masked";
            var client = new SailthruClient(apiKey, secret);
            return client;
        }

        public async Task Should_send_identify_call(string userId, string email)
        {
            Options options = new Segment.Model.Options();
            Context context = new Segment.Model.Context()
            {
                {"update_location", "LoadTestMultipleIdentifyCalls"},
            };
            options.SetContext(context);
            options.SetAnonymousId(null);
            Traits traits = new Segment.Model.Traits()
            {
                {"first_name", "Load"},
                {"last_name", "Test"},
            };

            if (!string.IsNullOrEmpty(email))
                traits.Add("email", email);

            Console.WriteLine($"Sending identify call for userId = {userId}, email = {email}");
            Analytics.Client.Identify(userId, traits, options);
        }
        public async Task Should_send_track_call_with_null_user_id(string email)
        {
            Options options = new Segment.Model.Options();
            Traits traits = new Segment.Model.Traits()
            {
                {"email", email} // context.traits.email is what will create the user profile in Sailthru
            };
            Context context = new Segment.Model.Context()
            {
                {"update_location", "LoadTestTrackIdentifyRace"},
            };
            context.Add("traits", traits);
            options.SetContext(context);

            options.SetAnonymousId($"anon_{email}");
            Properties properties = new Segment.Model.Properties()
            {
                {"language", "en-ca"},
            };

            Console.WriteLine($"Sending track call for userId = null, and email = {email}");
            Analytics.Client.Track(null, "RayTest Ran", properties, options);
        }
       
        [TestMethod]
        public async Task Should_send_multiple_identify_and_track_call_pairs()
        {
            Initialize_Segment_with_standard_configuration();

            //-- arrange
            int numSets = 100;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            string userIdPrefix = $"IdentifyTrackPairsF_";
            for (int setNo = 1; setNo <= numSets; setNo++)
            {
                string userId = userIdPrefix + $"{setNo}"; // e.g. IdentifyTrackPairsC_3
                string email = userIdPrefix + $"{setNo}@indigo.ca"; // e.g. IdentifyTrackPairsC_3@indigo.ca

                //-- arrange and act
                await Should_send_identify_call(userId, email);

                // note that track call will also create user profile if it doesn't exist
                await Should_send_track_call_with_null_user_id(email); 
            }

            double time1 = sw.ElapsedMilliseconds;

            Analytics.Client.Flush();
            sw.Stop();
            double time2 = sw.ElapsedMilliseconds;

            //-- assert
            await Task.Delay(3000); // need to wait long enough for profiles and traits to be populated properly

            SailthruClient sailthruClient = GetSailthruClient();

            bool isExpected = Should_get_single_user_profile_per_set(sailthruClient, numSets, userIdPrefix);
            isExpected.Should().BeTrue();
        }

        public bool Should_get_single_user_profile_per_set(SailthruClient salithruClient, 
            int numSets, string userIdPrefix)
        {
            bool isExpected = true;

            for (int setNo = 1; setNo <= numSets; setNo++)
            {
                string userId = userIdPrefix + $"{setNo}";
                string email = userIdPrefix + $"{setNo}@indigo.ca";
                bool isProfileFound = true;

                //---- search by email
                UserRequest searchByEmailRequest = new UserRequest();
                searchByEmailRequest.Id = email;

                SailthruResponse searchByEmailResponse = salithruClient.GetUser(searchByEmailRequest);
                SailthruResponseUserProfile searchByEmailProfile = null;
                if (searchByEmailResponse.IsOK())
                {
                    Console.WriteLine($"Found user profile by email = {searchByEmailRequest.Id}");
                    var obj = JsonConvert.DeserializeObject(searchByEmailResponse.RawResponse);
                    var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    // Console.WriteLine(formattedJson);

                    searchByEmailProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(searchByEmailResponse.RawResponse);
                }
                else
                {
                    Console.WriteLine($"ERROR: User profile not found for email = {searchByEmailRequest.Id}");
                    Console.WriteLine(searchByEmailResponse.RawResponse);
                    isProfileFound = false;
                }

                //----- search by userId
                UserRequest searchByExtIdRequest = new UserRequest();
                searchByExtIdRequest.Id = userId;
                searchByExtIdRequest.Key = "extid";

                SailthruResponse searchByExtIdResponse = salithruClient.GetUser(searchByExtIdRequest);
                SailthruResponseUserProfile searchByExtIdProfile = null;
                if (searchByExtIdResponse.IsOK())
                {
                    Console.WriteLine($"Found user profile by extId = {searchByExtIdRequest.Id}");
                    var obj = JsonConvert.DeserializeObject(searchByExtIdResponse.RawResponse);
                    var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    // Console.WriteLine(formattedJson);

                    searchByExtIdProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(searchByExtIdResponse.RawResponse);
                }
                else
                {
                    Console.WriteLine($"User profile not found for userId = {searchByExtIdRequest.Id}");
                    Console.WriteLine(searchByExtIdResponse.RawResponse);
                    isProfileFound = false;
                }

                //----- compare the two responses
                if (isProfileFound)
                {
                    isExpected = isExpected && Should_compare_two_user_profiles_should_be_same(
                        searchByEmailProfile, searchByExtIdProfile);
                }
            }
            return isExpected;
        }

        public bool Should_compare_two_user_profiles_should_be_same(
            SailthruResponseUserProfile emailProfile, SailthruResponseUserProfile extIdProfile)
        {
            bool isExpected = true;

            if (emailProfile == null || extIdProfile == null)
                isExpected = false;

            // check email, extId, and sid are the same -- which means single profile. different = two profiles
            if (isExpected &&
                emailProfile.Overview.Email != extIdProfile.Overview.Email && 
                emailProfile.Overview.ExtId != extIdProfile.Overview.ExtId &&
                emailProfile.Overview.Sid != extIdProfile.Overview.Sid)
            {
                Console.WriteLine($"Error - Sailthru created TWO profiles");
                Console.WriteLine($"      - profileWithEmail: email={emailProfile.Overview.Email}, extId={emailProfile.Overview.ExtId}, sid={emailProfile.Overview.Sid}");
                Console.WriteLine($"      - profileWithExtId: email={extIdProfile.Overview.Email}, extId={extIdProfile.Overview.ExtId}, sid={extIdProfile.Overview.Sid}");
                isExpected = false;
            }

            // check traits 
            if (isExpected &&
                emailProfile.UserTraits.FirstName != "Load" &&
                emailProfile.UserTraits.LastName != "Test")
            {
                isExpected = false;
                Console.WriteLine($"Error - Sailthru created ONE profile, but user traits not as expected");
            }

            if (!isExpected)
            {
                // print results
                var emailProfileJson = JsonConvert.SerializeObject(emailProfile, Formatting.Indented);
                var extIdProfileJson = JsonConvert.SerializeObject(extIdProfile, Formatting.Indented);
                Console.WriteLine($"--- Profile With Email --- : \r\n{emailProfileJson}");
                Console.WriteLine($"--- Profile With ExtId --- : \r\n{extIdProfileJson}");
            }
            return isExpected;
        }



        //----------------------------------------------------------------------------
        // Segment Callback Handlers
        //----------------------------------------------------------------------------
        public void FailureHandler(BaseAction action, System.Exception e)
        {
            Console.WriteLine($"Failure: userid={action.UserId}"); 
            Console.WriteLine(e.ToString());
        }

        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"Success: userid={action.UserId}"); 
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {DateTime.Now.ToString("o")}{message}");
        }
    }
}
