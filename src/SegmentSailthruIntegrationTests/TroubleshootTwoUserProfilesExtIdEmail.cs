﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Sailthru;
using Sailthru.Models;
using Segment;
using Segment.Model;
using SegmentSailthruIntegrationTests.Models;

namespace SegmentSailthruIntegrationTests
{
    /// <summary>
    /// The purpose of this suite of test cases is to troubleshoot a production issue.
    /// 
    /// During Indigo.ca online account signup with regular plum membership
    /// We send two identify calls
    /// 1. One for "Online – AddLoyaltyCard" to create the account with membership
    /// 2. One for "AddCustomerPreference" to opt-in or out of marketing emails
    /// 
    /// In Snowflake, we see
    /// 1. makes Identify() call with customerId/userId/extId, loyaltynumber, but EMPTY email
    /// 2. makes Identify() call with customerId/userId/extId, opt_out_status, and with email
    /// 
    /// In Sailthru, we see
    /// 1. profile created with extId but NO email
    /// 2. profile created with NO extId, but WITH email
    /// 
    /// We expect only one profile to be created.
    /// The two identify requests are sent simultaneously, and we suspect there could be a race condition in Sailthru handling the Segment events.
    /// </summary>
    [TestClass]
    public class TroubleshootTwoUserProfilesExtIdEmail
    { 

        public void Initialize_Segment_with_standard_configuration()
        {
            string writeKey = "masked"; // make sure to pick the correct environment/source!
            int maxQueueSize = 100000;
            int flushThreshold = 40;
            bool enableGzip = false;
            bool enableBatching = true; 

            Config config = new Config()
                .SetMaxQueueSize(maxQueueSize)
                .SetFlushAt(flushThreshold)
                .SetGzip(enableGzip)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        public SailthruClient GetSailthruClient()
        {
            // Log into Sailthru first (make sure to pick the correct environment)
            // then you can find the keys here: https://my.sailthru.com/settings/api_postbacks#
            string apiKey = "masked"; 
            string secret = "masked";
            var client = new SailthruClient(apiKey, secret);
            return client;
        }

        public async Task Should_send_identify_call_with_loyalty_number(string userId, string email)
        {
            #region Sample Segment Json from QA Rocky
            /*
            {
                "anonymousId": null,
                "context": {
                "app": "CustDBServices",
                "library": {
                    "name": "Analytics.NET",
                    "version": "3.8.0"
                },
                "update_location": "Online - AddLoyaltyCard",
                "version": "4.17.1.456"
                },
                "integrations": {},
                "messageId": "87797c16-3642-42cf-8aa6-a39d94a8e7c9",
                "originalTimestamp": "2022-01-18T20:21:06.6295387-05:00",
                "receivedAt": "2022-01-19T01:21:07.891Z",
                "sentAt": "2022-01-19T01:21:07.300Z",
                "timestamp": "2022-01-19T01:21:07.219Z",
                "traits": {
                    "loyalty_number": 590008433055,
                    "loyalty_subscription_expiry_date": null,
                    "loyalty_subscription_last_renewal_date": null,
                    "loyalty_type": "plum"
                },
                "type": "identify",
                "userId": "176016114",
                "writeKey": "*masked*"
            }
            */
            #endregion

            Options options = new Segment.Model.Options();
            Context context = new Segment.Model.Context()
            {
                {"update_location", "TroubleshootTwoUserProfilesExtIdEmail_AddLoyalty"},
            };
            options.SetContext(context);
            options.SetAnonymousId(null);
            Traits traits = new Segment.Model.Traits()
            {
                {"loyalty_number", "590008433055"},
                {"loyalty_subscription_expiry_date", null},
                {"loyalty_subscription_last_renewal_date", null},
                {"loyalty_type", "plum"},
            };

            if (!string.IsNullOrEmpty(email))
                traits.Add("email", email);

            Console.WriteLine($"Sending identify call for userId = {userId} to Add Loyalty Card");
            Analytics.Client.Identify(userId, traits, options);
        }

        public async Task Should_send_identify_call_to_set_opt_out_status(string userId, string email)
        {
            #region Sample Segment Json from QA Rocky
            /*
            {
              "anonymousId": null,
              "context": {
                "app": "CustDBServices",
                "library": {
                  "name": "Analytics.NET",
                  "version": "3.8.0"
                },
                "update_location": "AddCustomerWithPreferencesCall",
                "version": "4.17.1.456"
              },
              "integrations": {},
              "messageId": "425969a4-0967-4a17-9ee0-47b22d91b423",
              "originalTimestamp": "2022-01-18T20:21:06.1449293-05:00",
              "receivedAt": "2022-01-19T01:21:07.891Z",
              "sentAt": "2022-01-19T01:21:07.300Z",
              "timestamp": "2022-01-19T01:21:06.734Z",
              "traits": {
                "birthday": null,
                "created_at": "2022-01-18T20:21:06.1449293",
                "email": "mskeochy4@indigo.ca",
                "first_name": null,
                "language": "en-ca",
                "last_name": null,
                "membership_id": "691d1553-d9a1-4ab9-8cb3-19516ff52764",
                "opt_out_status": "optout_none"
              },
              "type": "identify",
              "userId": "176016114",
              "writeKey": "*masked*"
            }
             */
            #endregion

            Options options = new Segment.Model.Options();
            Context context = new Segment.Model.Context()
            {
                {"update_location", "TroubleshootTwoUserProfilesExtIdEmail_UpdateCustPref"},
            };
            options.SetContext(context);
            options.SetAnonymousId(null);
            Traits traits = new Segment.Model.Traits()
            {
                {"birthday", null},
                {"created_at", DateTime.Now },
                {"email", email},
                {"first_name", "Ray"},
                {"language", "en-ca"},
                {"last_name", "Test"},
                {"membership_id", "691d1553-d9a1-4ab9-8cb3-19516ff52764"},
                {"opt_out_status", "optout_none"}
            };

            Console.WriteLine($"Sending identify call for userId = {userId} and email = {email} to Opt Out Status = optout_none");
            Analytics.Client.Identify(userId, traits, options);
        }

        [TestMethod]
        public async Task Should_send_two_identify_calls_to_simulate_new_account_opening()
        {
            Initialize_Segment_with_standard_configuration();

            //-- arrange
            string userId = "rayTest01";
            string email = "rayTest01@indigo.ca";

            //-- arrange and act
            await Should_send_identify_call_with_loyalty_number(userId, email: ""); // first call has no email
            await Should_send_identify_call_to_set_opt_out_status(userId, email);

            Analytics.Client.Flush();


            //-- assert
            await Task.Delay(3000); // wait to give it some time (2s was not enough)

            await Should_find_Sailthru_user_profile(userId, email, isPrintJson: true);

        }

        [TestMethod]
        public async Task Should_batch_test_two_identify_calls_to_simulate_new_account_opening()
        {
            Initialize_Segment_with_standard_configuration();

            //-- arrange
            int numCalls = 80;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 1; i <= numCalls; i++)
            {
                string userId = $"raytestM{i}";
                string email = $"raytestM{i}@indigo.ca";

                //-- arrange and act
                await Should_send_identify_call_with_loyalty_number(userId, email); // pass in string.empty if we want first call to NOT have email
                await Should_send_identify_call_to_set_opt_out_status(userId, email);

            }
            double time1 = sw.ElapsedMilliseconds;

            Analytics.Client.Flush();
            sw.Stop();
            double time2 = sw.ElapsedMilliseconds;

            Console.WriteLine($"---- w Batching 'flushAt=40', default 'flushInterval=10s' ----");
            Console.WriteLine($"---- {numCalls} SETS = {numCalls * 2} identify calls. time fire-forget {time1} ms, time flush {time2} ms");

            //-- assert
            await Task.Delay(4000);

            var resultList = new Dictionary<string, bool>();
            for (int i = 1; i <= numCalls; i++)
            {
                string userId = $"raytestM{i}";
                string email = $"raytestM{i}@indigo.ca";
                bool isFoundCorrectly = await Should_find_Sailthru_user_profile(userId, email, isPrintJson: false);
                resultList.Add(userId, isFoundCorrectly);
            }

            string summaryMessage = "";
            foreach (string key in resultList.Keys)
            {
                if (resultList[key] != true)
                {
                    summaryMessage += $"Error found with userId={key}" + Environment.NewLine;
                }
                
            }

            if (String.IsNullOrEmpty(summaryMessage))
            {
                Console.WriteLine("No Errors Found");
            }
            else
            {
                Console.WriteLine(summaryMessage);
                Assert.Fail("Errors were found with the Sailthru Profile" + Environment.NewLine + summaryMessage);
            }
            
        }
        //------------------------------------------------------------------------------------------------------------------------
        [TestMethod]
        public async Task Should_find_Sailthru_user_profile()
        {
            string userId = "rayTest01";
            string email = "rayTest01@indigo.ca";
            bool isFoundCorrectly = await Should_find_Sailthru_user_profile(userId, email, isPrintJson: true);
            isFoundCorrectly.Should().BeTrue();
        }

        public async Task<bool> Should_find_Sailthru_user_profile(string userId, string email, bool isPrintJson)
        {
            SailthruClient sailthruClient = GetSailthruClient();

            //-- search by extId
            var queryByExtId = new UserRequest();
            queryByExtId.Id = userId;
            queryByExtId.Key = "extid"; 

            SailthruResponse response = sailthruClient.GetUser(queryByExtId);
            SailthruResponseUserProfile profileByExtId = GetAndPrintUserProfile(response, queryByExtId.Id, queryByExtId.Key, isPrintJson);

            bool isValidProfileByExtId = Should_have_expected_profiles(profileByExtId, userId, email, "for query by extId");

            //-- search by email
            bool isValidProfileByEmail = true;
            if (!string.IsNullOrEmpty(email))
            {
                var queryByEmail = new UserRequest();
                queryByEmail.Id = email;
                queryByEmail.Key = "email";

                response = sailthruClient.GetUser(queryByEmail);
                SailthruResponseUserProfile profileByEmail =
                    GetAndPrintUserProfile(response, queryByEmail.Id, queryByEmail.Key, isPrintJson);

                //-- assert - make sure both users exist and are the same
                profileByExtId.Should().NotBeNull($"Could not find user profile by extId {userId}, email = {email}");
                profileByEmail.Should().NotBeNull($"Could not find user profile by email {userId}, email = {email}");
                isValidProfileByEmail = Should_have_expected_profiles(profileByEmail, userId, email, "for query by email");
            }

            return isValidProfileByExtId && isValidProfileByEmail;
        }

        public bool Should_have_expected_profiles(SailthruResponseUserProfile p, string userId, string email, string comment)
        {
            Console.WriteLine($"Validating profile {comment} with userId={userId}, email={email}... ");

            string errorMessage = "";

            if (!string.IsNullOrEmpty(email)) // only check if expected email is supplied
            {
                if (p.Overview.Email != email.ToLower())
                    errorMessage += $"   email error. actual {p.Overview.Email}, expected {email.ToLower()}" + Environment.NewLine;
            }

            if (p.Overview.ExtId != userId)
                errorMessage += $"   userId/extId error. actual {p.Overview.ExtId}, expected {userId}" + Environment.NewLine;
            if (p.UserTraits.FirstName != "Ray")
                errorMessage += $"   first_name error. actual {p.UserTraits.FirstName}, expected Ray" + Environment.NewLine;
            if (!p.UserTraits.LoyaltyNumber.HasValue)
                errorMessage += $"   loyalty_number error. actual {p.UserTraits.LoyaltyNumber.GetValueOrDefault()}, expected 590008433055" + Environment.NewLine;
            if (p.UserTraits.OptOutStatus != "optout_none")
                errorMessage += $"   opt_out_status error. actual {p.UserTraits.OptOutStatus}, expected optout_none" + Environment.NewLine;
            if (p.OptoutEmail != "none") 
                errorMessage += $"   OptoutEmail error. actual {p.OptoutEmail}, expected none" + Environment.NewLine;

            if (!string.IsNullOrEmpty(errorMessage))
            {
                Console.WriteLine(errorMessage);
                return false;
            }
            
            Console.WriteLine("   DONE, user profile is valid.");
            return true;
        }

        public SailthruResponseUserProfile GetAndPrintUserProfile(SailthruResponse response, string queryId, string queryKey, bool isPrintJson)
        {
            SailthruResponseUserProfile profile = null;

            if (response.IsOK())
            {
                if (isPrintJson)
                {
                    Console.WriteLine($"User profile found for {queryKey} = {queryId}");
                    var obj = JsonConvert.DeserializeObject(response.RawResponse);
                    var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    Console.WriteLine(formattedJson);
                }
                profile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(response.RawResponse);
            }
            else
            {
                Console.WriteLine($"User profile NOT found for {queryKey} = {queryId}");
                Console.WriteLine(response.RawResponse);
            }

            return profile;
        }



        //----------------------------------------------------------------------------
        // Segment Callback Handlers
        //----------------------------------------------------------------------------
        public void FailureHandler(BaseAction action, System.Exception e)
        {
            Console.WriteLine($"Failure: userid={action.UserId}"); 
            Console.WriteLine(e.ToString());
        }

        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"Success: userid={action.UserId}"); 
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {DateTime.Now.ToString("o")}{message}");
        }
    }
}
