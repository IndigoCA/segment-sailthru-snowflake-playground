﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Sailthru;
using Sailthru.Models;
using Segment;
using Segment.Model;
using SegmentSailthruIntegrationTests.Models;

namespace SegmentSailthruIntegrationTests
{
    /// <summary>
    ///This integration test runner is to verify behavior of Sailthru's implementation where Track() calls create user profiles
    /// </summary>
    [TestClass] // uncomment to run
    public class TrackCallCreateSailthruUserProfile
    { 

        public void Initialize_Segment_with_standard_configuration()
        {
            string writeKey = "masked"; // make sure to pick the correct environment/source!
            int maxQueueSize = 100000;
            int flushThreshold = 40;
            bool enableGzip = false;
            bool enableBatching = true; 

            Config config = new Config()
                .SetMaxQueueSize(maxQueueSize)
                .SetFlushAt(flushThreshold)
                .SetGzip(enableGzip)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        public SailthruClient GetSailthruClient()
        {
            // Log into Sailthru first (make sure to pick the correct environment)
            // then you can find the keys here: https://my.sailthru.com/settings/api_postbacks#
            string apiKey = "masked"; 
            string secret = "masked";
            var client = new SailthruClient(apiKey, secret);
            return client;
        }
        
        public async Task Should_send_track_call(string userId, string email)
        {
            Options options = new Segment.Model.Options();
            Traits traits = new Segment.Model.Traits()
            {
                {"email", email} // context.traits.email is what will create the user profile in Sailthru
            };
            Context context = new Segment.Model.Context()
            {
                {"update_location", "LoadTestTrackIdentifyRace"},
            };
            context.Add("traits", traits);
            options.SetContext(context);

            options.SetAnonymousId($"anon_{email}");
            Properties properties = new Segment.Model.Properties()
            {
                {"language", "en-ca"},
            };

            string userIdString = userId ?? null;
            Console.WriteLine($"Sending track call for userId = {userIdString}, and email = {email}");
            Analytics.Client.Track(userId, "RayTest Ran", properties, options);
        }

        //--------------------------------------------------------------------------------
        [TestMethod]
        public async Task Should_send_track_call_and_create_user_profile_with_specified_userId()
        {
            Initialize_Segment_with_standard_configuration();

            //-- arrange
            string userId = $"TrackCallCreateSailthruUserProfileExtIdSet_2";
            string email = userId + "@indigo.ca"; // e.g. TrackCallCreateSailthruUserProfile1@indigo.ca

            // make sure profile doesn't exist
            SailthruClient sailthruClient = GetSailthruClient();
            Should_not_find_user_profile(sailthruClient, userId, email);

            //-- act
            await Should_send_track_call(userId, email);
            Analytics.Client.Flush();

            //-- assert
            await Task.Delay(3000); // need to wait long enough for Sailthru User Profile to be created

            Should_not_find_user_profile(sailthruClient, userId, email);
        }

        [TestMethod]
        public async Task Should_send_track_call_and_create_user_profile_with_null_userId()
        {
            Initialize_Segment_with_standard_configuration();

            //-- arrange
            string userId = null;
            string email = "TrackCallCreateSailthruUserProfileExtIdNull_3@indigo.ca";

            // make sure profile doesn't exist
            SailthruClient sailthruClient = GetSailthruClient();
            Should_not_find_user_profile(sailthruClient, userId, email);

            //-- act
            await Should_send_track_call(userId, email);
            Analytics.Client.Flush();

            //-- assert
            await Task.Delay(3000); // need to wait long enough for Sailthru User Profile to be created

            Should_get_single_user_profile_with_extId_null(sailthruClient, email);
        }

        //-------------------------------------------------------------
        public bool Should_not_find_user_profile(SailthruClient salithruClient, string userId, string email)
        {
            bool isProfileFound = false;

            //---- search by email
            UserRequest searchByEmailRequest = new UserRequest();
            searchByEmailRequest.Id = email;

            SailthruResponse searchByEmailResponse = salithruClient.GetUser(searchByEmailRequest);
            if (searchByEmailResponse.IsOK())
            {
                isProfileFound = true;
                Console.WriteLine($"ERROR: Found (but should not have) user profile by email {searchByEmailRequest.Id}");

                var obj = JsonConvert.DeserializeObject(searchByEmailResponse.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                Console.WriteLine(formattedJson);
            }

            //----- search by userId
            if (userId == null)
                return isProfileFound;

            UserRequest searchByExtIdRequest = new UserRequest();
            searchByExtIdRequest.Id = userId;
            searchByExtIdRequest.Key = "extid";

            SailthruResponse searchByExtIdResponse = salithruClient.GetUser(searchByExtIdRequest);
            if (searchByExtIdResponse.IsOK())
            {
                isProfileFound = true;
                Console.WriteLine($"ERROR: Found (but should not have) user profile by extId {userId}");

                var obj = JsonConvert.DeserializeObject(searchByExtIdResponse.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                Console.WriteLine(formattedJson);
            }

            return isProfileFound;
        }

        public void Should_get_single_user_profile_with_extId_null(SailthruClient salithruClient, string email)
        {
            //---- search by email
            UserRequest searchByEmailRequest = new UserRequest();
            searchByEmailRequest.Id = email;

            SailthruResponse searchByEmailResponse = salithruClient.GetUser(searchByEmailRequest);
            if (searchByEmailResponse.IsOK())
            {
                var obj = JsonConvert.DeserializeObject(searchByEmailResponse.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                Console.WriteLine("Found user profile by email:");
                Console.WriteLine(formattedJson);
            }
            else
            {
                Console.WriteLine($"ERROR: User profile not found for email = {searchByEmailRequest.Id}");
                Console.WriteLine(searchByEmailResponse.RawResponse);
                Assert.Fail($"ERROR: User profile not found for email = { searchByEmailRequest.Id}");
            }
        }

        /* track call should NOT be creating user profiles if user id is specified
        public void Should_get_single_user_profile_with_extId_set(SailthruClient salithruClient, string userId, string email)
        {
            bool isProfileFound = true;

            //---- search by email
            UserRequest searchByEmailRequest = new UserRequest();
            searchByEmailRequest.Id = email;

            SailthruResponse searchByEmailResponse = salithruClient.GetUser(searchByEmailRequest);
            SailthruResponseUserProfile searchByEmailProfile = null;
            if (searchByEmailResponse.IsOK())
            {
                var obj = JsonConvert.DeserializeObject(searchByEmailResponse.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                // Console.WriteLine(formattedJson);
                searchByEmailProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(searchByEmailResponse.RawResponse);
            }
            else
            {
                Console.WriteLine($"ERROR: User profile not found for email = {searchByEmailRequest.Id}");
                Console.WriteLine(searchByEmailResponse.RawResponse);
                isProfileFound = false;
            }

            //----- search by userId
            UserRequest searchByExtIdRequest = new UserRequest();
            searchByExtIdRequest.Id = userId;
            searchByExtIdRequest.Key = "extid";

            SailthruResponse searchByExtIdResponse = salithruClient.GetUser(searchByExtIdRequest);
            SailthruResponseUserProfile searchByExtIdProfile = null;
            if (searchByExtIdResponse.IsOK())
            {
                var obj = JsonConvert.DeserializeObject(searchByExtIdResponse.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                // Console.WriteLine(formattedJson);
                searchByExtIdProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(searchByExtIdResponse.RawResponse);
            }
            else
            {
                Console.WriteLine($"ERROR: User profile not found for userId = {searchByExtIdRequest.Id}");
                Console.WriteLine(searchByExtIdResponse.RawResponse);
                isProfileFound = false;
            }

            // make sure both profiles are the same
            if (isProfileFound)
            {
                Should_compare_two_user_profiles_should_be_same(searchByEmailProfile, searchByExtIdProfile);
            }
            else
                Assert.Fail($"User Profile not found by at least one of email = {searchByEmailRequest.Id} or userId = {searchByExtIdRequest.Id}");
        }
        */
        public void Should_compare_two_user_profiles_should_be_same(SailthruResponseUserProfile emailProfile, SailthruResponseUserProfile extIdProfile)
        {
            emailProfile.Should().NotBeNull("Profile by email is null, should not be");
            extIdProfile.Should().NotBeNull("Profile by extId is null, should not be");

            // check email, extId, and sid are the same -- which means single profile. different = two profiles
            if (emailProfile.Overview.Email != extIdProfile.Overview.Email &&
                emailProfile.Overview.ExtId != extIdProfile.Overview.ExtId &&
                emailProfile.Overview.Sid != extIdProfile.Overview.Sid)
            {
                Console.WriteLine($"Error - Sailthru created TWO profiles");
                Console.WriteLine($"      - profileWithEmail: email={emailProfile.Overview.Email}, extId={emailProfile.Overview.ExtId}, sid={emailProfile.Overview.Sid}");
                Console.WriteLine($"      - profileWithExtId: email={extIdProfile.Overview.Email}, extId={extIdProfile.Overview.ExtId}, sid={extIdProfile.Overview.Sid}");

                // print results
                var emailProfileJson = JsonConvert.SerializeObject(emailProfile, Formatting.Indented);
                var extIdProfileJson = JsonConvert.SerializeObject(extIdProfile, Formatting.Indented);
                Console.WriteLine($"--- Profile With Email --- : \r\n{emailProfileJson}");
                Console.WriteLine($"--- Profile With ExtId --- : \r\n{extIdProfileJson}");

                Assert.Fail("Sailthru created TWO profiles");
            }
        }
        //----------------------------------------------------------------------------
        // Segment Callback Handlers
        //----------------------------------------------------------------------------
        public void FailureHandler(BaseAction action, System.Exception e)
        {
            Console.WriteLine($"Failure: userid={action.UserId}"); 
            Console.WriteLine(e.ToString());
        }

        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"Success: userid={action.UserId}"); 
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {DateTime.Now.ToString("o")}{message}");
        }
    }
}
