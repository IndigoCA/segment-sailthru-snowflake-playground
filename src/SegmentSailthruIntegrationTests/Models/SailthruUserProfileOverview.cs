﻿using Newtonsoft.Json;

namespace SegmentSailthruIntegrationTests.Models
{
    /// <summary>
    /// This class maps to Sailthru User Profile Overview page
    /// </summary>
    public class SailthruUserProfileOverview
    {
        /// <summary>
        /// Sailthru external ID maps to our Indigo CustomerId
        /// </summary>
        [JsonProperty(PropertyName = "extId")]
        public string ExtId { get; set; }

        /// <summary>
        /// Sailthru's unique user profile id
        /// </summary>
        [JsonProperty(PropertyName = "sid")]
        public string Sid { get; set; }


        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        // public string cookie { get; set; }

    }
}

