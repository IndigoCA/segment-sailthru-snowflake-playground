﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Sailthru;
using Sailthru.Models;
using Segment;
using Segment.Model;
using SegmentSailthruIntegrationTests.Models;

namespace SegmentSailthruIntegrationTests
{
    /// <summary>
    /// The purpose of this suite of test cases load test sending multiple identify()
    /// calls with same userId but with same email, but different traits.
    /// In production, we've seen some cases where Sailthru creates two profiles
    /// one with extid and no email
    /// one with email and no extid
    ///
    /// This test class only adds traits.
    /// No errors found running this.
    /// </summary>
    // [TestClass] // uncomment to run
    public class LoadTestMultipleIdentifyCalls_BasicTraits
    {

        public void Initialize_Segment_with_standard_configuration()
        {
            string writeKey = "masked"; // make sure to pick the correct environment/source!
            int maxQueueSize = 100000;
            int flushThreshold = 40;
            bool enableGzip = false;
            bool enableBatching = true;

            Config config = new Config()
                .SetMaxQueueSize(maxQueueSize)
                .SetFlushAt(flushThreshold)
                .SetGzip(enableGzip)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        public SailthruClient GetSailthruClient()
        {
            // Log into Sailthru first (make sure to pick the correct environment)
            // then you can find the keys here: https://my.sailthru.com/settings/api_postbacks#
            string apiKey = "masked";
            string secret = "masked";
            var client = new SailthruClient(apiKey, secret);
            return client;
        }

        public async Task Should_send_identify_call(string userId, string email, string traitKey, string traitValue)
        {
            Options options = new Segment.Model.Options();
            Context context = new Segment.Model.Context()
            {
                {"update_location", "LoadTestMultipleIdentifyCalls"},
            };
            options.SetContext(context);
            options.SetAnonymousId(null);
            Traits traits = new Segment.Model.Traits()
            {
                {traitKey, traitValue},
            };

            if (!string.IsNullOrEmpty(email))
                traits.Add("email", email);

            Console.WriteLine($"Sending identify call for userId = {userId}, email = {email}, {traitKey} = {traitValue}");
            Analytics.Client.Identify(userId, traits, options);
        }


        [TestMethod]
        public async Task Should_send_multiple_identify_calls()
        {
            Initialize_Segment_with_standard_configuration();

            //-- arrange
            int numSets = 80;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            string userIdPrefix = $"MultipleIdentifyD_";
            for (int setNo = 1; setNo <= numSets; setNo++)
            {
                string userId = userIdPrefix + $"{setNo}";// e.g. MultipleIdentifyB_3
                string email = userIdPrefix + $"{setNo}@indigo.ca"; // e.g. MultipleIdentifyB_3@indigo.ca

                for (int i = 1; i <= 7; i++) // hardcode to 7 user traits
                {
                    string traitKey = $"trait_{i}";
                    string traitValue = $"value_{i}";

                    //-- arrange and act
                    await Should_send_identify_call(userId, email, traitKey, traitValue);
                }
            }

            double time1 = sw.ElapsedMilliseconds;

            Analytics.Client.Flush();
            sw.Stop();
            double time2 = sw.ElapsedMilliseconds;

            //-- assert
            await Task.Delay(4000); // need to wait long enough for profiles and traits to be populated properly

            SailthruClient sailthruClient = GetSailthruClient();

            bool isErrorFindingOtherProfiles = Should_get_single_user_profile_per_set(sailthruClient, numSets, userIdPrefix);
            isErrorFindingOtherProfiles.Should().BeTrue();
        }

        public bool Should_get_single_user_profile_per_set(SailthruClient salithruClient, int numSets, string userIdPrefix)
        {
            bool isExpected = true;

            for (int setNo = 1; setNo <= numSets; setNo++)
            {
                string userId = userIdPrefix + $"{setNo}";
                string email = userIdPrefix + $"{setNo}@indigo.ca";
                bool isProfileFound = true;

                //---- search by email
                UserRequest searchByEmailRequest = new UserRequest();
                searchByEmailRequest.Id = email;

                SailthruResponse searchByEmailResponse = salithruClient.GetUser(searchByEmailRequest);
                SailthruResponseUserProfile searchByEmailProfile = null;
                if (searchByEmailResponse.IsOK())
                {
                    var obj = JsonConvert.DeserializeObject(searchByEmailResponse.RawResponse);
                    var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    // Console.WriteLine(formattedJson);

                    searchByEmailProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(searchByEmailResponse.RawResponse);
                }
                else
                {
                    Console.WriteLine($"ERROR: User profile not found for email = {searchByEmailRequest.Id}");
                    Console.WriteLine(searchByEmailResponse.RawResponse);
                    isProfileFound = false;
                }

                //----- search by userId
                UserRequest searchByExtIdRequest = new UserRequest();
                searchByExtIdRequest.Id = userId;
                searchByExtIdRequest.Key = "extid";


                SailthruResponse searchByExtIdResponse = salithruClient.GetUser(searchByExtIdRequest);
                SailthruResponseUserProfile searchByExtIdProfile = null;
                if (searchByExtIdResponse.IsOK())
                {
                    var obj = JsonConvert.DeserializeObject(searchByExtIdResponse.RawResponse);
                    var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    // Console.WriteLine(formattedJson);

                    searchByExtIdProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(searchByExtIdResponse.RawResponse);
                }
                else
                {
                    Console.WriteLine($"User profile not found for userId = {searchByExtIdRequest.Id}");
                    Console.WriteLine(searchByExtIdResponse.RawResponse);
                    isProfileFound = false;
                }

                //----- compare the two responses
                if (isProfileFound)
                {
                    isExpected = isExpected && Should_compare_two_user_profiles_should_be_same(
                        searchByEmailProfile, searchByExtIdProfile);
                }
            }
            return isExpected;
        }

        public bool Should_compare_two_user_profiles_should_be_same(
            SailthruResponseUserProfile emailProfile, SailthruResponseUserProfile extIdProfile)
        {
            bool isExpected = true;

            if (emailProfile == null || extIdProfile == null)
                isExpected = false;

            // check email, extId, and sid are the same -- which means single profile. different = two profiles
            if (isExpected &&
                emailProfile.Overview.Email != extIdProfile.Overview.Email && 
                emailProfile.Overview.ExtId != extIdProfile.Overview.ExtId &&
                emailProfile.Overview.Sid != extIdProfile.Overview.Sid)
            {
                Console.WriteLine($"Error - Sailthru created TWO profiles");
                Console.WriteLine($"      - profileWithEmail: email={emailProfile.Overview.Email}, extId={emailProfile.Overview.ExtId}, sid={emailProfile.Overview.Sid}");
                Console.WriteLine($"      - profileWithExtId: email={extIdProfile.Overview.Email}, extId={extIdProfile.Overview.ExtId}, sid={extIdProfile.Overview.Sid}");
                isExpected = false;
            }

            // check traits (we hardcoded to 7) - should only need to check one of the profiles
            if (isExpected &&
                (emailProfile.UserTraits.Trait1 != "value_1" ||
                emailProfile.UserTraits.Trait2 != "value_2" ||
                emailProfile.UserTraits.Trait3 != "value_3" ||
                emailProfile.UserTraits.Trait4 != "value_4" ||
                emailProfile.UserTraits.Trait5 != "value_5" ||
                emailProfile.UserTraits.Trait6 != "value_6" ||
                emailProfile.UserTraits.Trait7 != "value_7"))
            {
                isExpected = false;
                Console.WriteLine($"Error - Sailthru created ONE profile, but user traits not as expected");
            }

            if (!isExpected)
            {
                // print results
                var emailProfileJson = JsonConvert.SerializeObject(emailProfile, Formatting.Indented);
                var extIdProfileJson = JsonConvert.SerializeObject(extIdProfile, Formatting.Indented);
                Console.WriteLine($"--- Profile With Email --- : \r\n{emailProfileJson}");
                Console.WriteLine($"--- Profile With ExtId --- : \r\n{extIdProfileJson}");
            }
            return isExpected;
        }



        //----------------------------------------------------------------------------
        // Segment Callback Handlers
        //----------------------------------------------------------------------------
        public void FailureHandler(BaseAction action, System.Exception e)
        {
            Console.WriteLine($"Failure: userid={action.UserId}"); 
            Console.WriteLine(e.ToString());
        }

        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"Success: userid={action.UserId}"); 
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {DateTime.Now.ToString("o")}{message}");
        }
    }
}
