﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Sailthru;
using Sailthru.Models;
using Segment;
using Segment.Model;
using SegmentSailthruIntegrationTests.Models;

namespace SegmentSailthruIntegrationTests
{
    /// <summary>
    /// The purpose of this suite of test cases load test sending multiple identify() calls with same userId but with different email addresses
    /// </summary>
    // [TestClass] // uncomment to run
    public class LoadTestMultipleEmailChanges
    { 

        public void Initialize_Segment_with_standard_configuration()
        {
            string writeKey = "masked"; // make sure to pick the correct environment/source!
            int maxQueueSize = 100000;
            int flushThreshold = 40;
            bool enableGzip = false;
            bool enableBatching = true; 

            Config config = new Config()
                .SetMaxQueueSize(maxQueueSize)
                .SetFlushAt(flushThreshold)
                .SetGzip(enableGzip)
                .SetAsync(enableBatching);

            Segment.Analytics.Initialize(writeKey, config);

            // setup callback handlers
            Segment.Analytics.Client.Failed += FailureHandler;
            Segment.Analytics.Client.Succeeded += SuccessHandler;
            Segment.Logger.Handlers += LogHandler;
        }

        public SailthruClient GetSailthruClient()
        {
            // Log into Sailthru first (make sure to pick the correct environment)
            // then you can find the keys here: https://my.sailthru.com/settings/api_postbacks#
            string apiKey = "masked"; 
            string secret = "masked";
            var client = new SailthruClient(apiKey, secret);
            return client;
        }

        public async Task Should_send_identify_call(string userId, string email, string firstName)
        {
            Options options = new Segment.Model.Options();
            Context context = new Segment.Model.Context()
            {
                {"update_location", "LoadTestMultipleEmailChanges"},
            };
            options.SetContext(context);
            options.SetAnonymousId(null);
            Traits traits = new Segment.Model.Traits()
            {
                {"first_name", firstName},
            };

            if (!string.IsNullOrEmpty(email))
                traits.Add("email", email);

            Console.WriteLine($"Sending identify call for userId = {userId}, email = {email}, first_name = {firstName}");
            Analytics.Client.Identify(userId, traits, options);
        }


        [TestMethod]
        public async Task Should_send_multiple_identify_calls()
        {
            Initialize_Segment_with_standard_configuration();

            //-- arrange
            int numCalls = 20;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 1; i <= numCalls; i++)
            {
                string userId = $"MultiplesTest";
                string email = $"MultiplesTest{i}@indigo.ca";
                string firstName = $"name_{i}";

                //-- arrange and act
                await Should_send_identify_call(userId, email, firstName); 

            }
            double time1 = sw.ElapsedMilliseconds;

            Analytics.Client.Flush();
            sw.Stop();
            double time2 = sw.ElapsedMilliseconds;

            //-- assert
            await Task.Delay(4000);


            SailthruClient sailthruClient = GetSailthruClient();

            bool isErrorFindingOtherProfiles = Should_not_find_other_profiles(sailthruClient, numCalls);

            string expectedUserId = "MultiplesTest";
            string expectedEmail = $"MultiplesTest{numCalls}@indigo.ca";
            string expectedFirstName = $"name_{numCalls}";
            bool isError = Should_get_last_user(sailthruClient, expectedUserId, expectedEmail, expectedFirstName);


            isErrorFindingOtherProfiles.Should().BeFalse();
            isError.Should().BeTrue();
        }

        public bool Should_not_find_other_profiles(SailthruClient salithruClient, int numCalls)
        {
            bool isExpected = true;

            // search by email
            for (int i=1; i<numCalls; i++) // one less than numCalls (e.g. 1-19 if numCalls = 20)
            {
                UserRequest userRequest = new UserRequest();
                userRequest.Id = $"MultiplesTest{i}@indigo.ca";

                SailthruResponse response = salithruClient.GetUser(userRequest);
                if (response.IsOK())
                {
                    Console.WriteLine($"Error - did NOT expect to find this user profile");
                    isExpected = false;

                    var obj = JsonConvert.DeserializeObject(response.RawResponse);
                    var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    Console.WriteLine(formattedJson);

                    SailthruResponseUserProfile userProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(response.RawResponse);
                }
            }
            return isExpected;
        }

        public bool Should_get_last_user(SailthruClient salithruClient, string expectedUserdId, string expectedEmail, string expectedFirstName)
        {
            UserRequest userRequest = new UserRequest();
            userRequest.Id = expectedUserdId;
            userRequest.Key = "extid";

            bool isExpected = true;

            SailthruResponse response = salithruClient.GetUser(userRequest);
            if (response.IsOK())
            {
                var obj = JsonConvert.DeserializeObject(response.RawResponse);
                var formattedJson = JsonConvert.SerializeObject(obj, Formatting.Indented);
                Console.WriteLine(formattedJson);

                SailthruResponseUserProfile userProfile = JsonConvert.DeserializeObject<SailthruResponseUserProfile>(response.RawResponse);
                var userProfileJson = JsonConvert.SerializeObject(userProfile, Formatting.Indented);
                
                if (userProfile.UserTraits.FirstName != expectedFirstName)
                {
                    Console.WriteLine($"Error - expected first_name {expectedFirstName} but found {userProfile.UserTraits.FirstName}");
                    isExpected = false;
                }

                if (userProfile.Overview.Email != expectedEmail)
                {
                    Console.WriteLine($"Error - expected email {expectedEmail} but found {userProfile.Overview.ExtId}");
                    isExpected = false;
                }
            }
            else
            {
                Console.WriteLine($"User profile not found for userId = {userRequest.Id}");
                Console.WriteLine(response.RawResponse);
                isExpected = false;
            }

            return isExpected;
        }

        //------------------------------------------------------------------------------------------------------------------------




        //----------------------------------------------------------------------------
        // Segment Callback Handlers
        //----------------------------------------------------------------------------
        public void FailureHandler(BaseAction action, System.Exception e)
        {
            Console.WriteLine($"Failure: userid={action.UserId}"); 
            Console.WriteLine(e.ToString());
        }

        public void SuccessHandler(BaseAction action)
        {
            Console.WriteLine($"Success: userid={action.UserId}"); 
        }
        public void LogHandler(Logger.Level level, string message, IDictionary<string, object> args)
        {
            if (args != null)
                message = args.Keys.Aggregate(message,
                    (current, key) => current + $" {"" + key}: {"" + args[key]},");

            Console.WriteLine($"[{level}] {DateTime.Now.ToString("o")}{message}");
        }
    }
}
